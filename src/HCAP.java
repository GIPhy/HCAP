/*
  ########################################################################################################

  HCAP: Homogeneous Composition Among Positions within FASTQ files
    
  Copyright (C) 2023  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.zip.*;

public class HCAP {

    //### constants  ################################################################
    final static String VERSION = "0.1.230815                               Copyright (C) 2023 Institut Pasteur";
    static final String NOTHING = "N.o./.T.h.I.n.G";
    static final    int BUFFER  = 1<<16;
    static final  short S_1     = (short) -1;
    static final   byte UTF8_A  = (byte) 65;
    static final   byte UTF8_C  = (byte) 67;
    static final   byte UTF8_G  = (byte) 71;
    static final   byte UTF8_T  = (byte) 84;
    static final   byte UTF8_a  = (byte) 97;
    static final   byte UTF8_c  = (byte) 99;
    static final   byte UTF8_g  = (byte) 103;
    static final   byte UTF8_t  = (byte) 116;
    static final   long MODMASK = 8191L; //## NOTE: see http://graphics.stanford.edu/~seander/bithacks.html#ModulusDivisionEasy
    static final String H1000   = "########################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################################";
    static final String N1000   = "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN";
    
    //### options   #################################################################
    static    File infq;    // -i
    static    File infq1;   // -1
    static    File infq2;   // -2
    static  String outname; // -o
    static boolean outgz;   // -z
    static  double cutoff;  // -c
    static   short minlgt;  // -l
    static boolean nocrop;  // -x
    static boolean verbose; // -v

    //### io   ######################################################################
    static         String filename;
    static BufferedReader in, in1, in2;
    static BufferedWriter out, out1, out2;

    //### data   ####################################################################
    static boolean   paired;             // true when PE reads
    static  String   l_1, l1_1, l2_1;    // line 1 of FASTQ block
    static  String   l_2, l1_2, l2_2;    // line 2 of FASTQ block
    static  String   l_3, l1_3, l2_3;    // line 3 of FASTQ block
    static  String   l_4, l1_4, l2_4;    // line 4 of FASTQ block
    static   short   mlgt;               // max read length
    static   short   hlgt;               // half max read length
    static    long[] fA,  fC,  fG,  fT;  // freq ACGT
    static    long[] fA1, fC1, fG1, fT1; // freq ACGT
    static    long[] fA2, fC2, fG2, fT2; // freq ACGT
    static    long[] np,  np1, np2;      // no. parsed pos.
    static  double   mA,  mC,  mG,  mT;  // median freq ACGT
    static  double   mA1, mC1, mG1, mT1; // median freq ACGT
    static  double   mA2, mC2, mG2, mT2; // median freq ACGT

    //### stuffs   ##################################################################
    static short p, pl, pl1, pl2, pr, pr1, pr2, up, lgt, lgt1, lgt2;
    static int o;
    static long cpt;
    static double n, sum;
    static boolean crop, discard, maybe;
    static byte[] ba, ba1, ba2;    
    static double[] zS, zS1, zS2, thr, thr1, thr2;

    public static void main(String[] args) throws IOException {

	//#############################################################################################################
	//#############################################################################################################
	//### doc                                                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println("HCAP v" + VERSION);
	    System.out.println("");
	    System.out.println(" Cropping FASTQ short reads to reach homogeneous composition among positions");
	    System.out.println("");
	    System.out.println(" USAGE:");
	    System.out.println("   HCAP -i <FASTQ>                  -o <basefile> [-z] [-l <int>] [-n] [-v] [-h]");
	    System.out.println("   HCAP -1 <FASTQ.R1> -2 <FASTQ.R2> -o <basefile> [-z] [-l <int>] [-n] [-v] [-h]");
	    System.out.println("");
	    System.out.println(" OPTIONS:");
	    System.out.println("    -i <infile>   FASTQ-formatted input file; filename should end with .gz when gzipped");
	    System.out.println("    -i <infile>   FASTQ-formatted R1 input file; filename should end with .gz when gzipped");
	    System.out.println("    -i <infile>   FASTQ-formatted R2 input file; filename should end with .gz when gzipped");
	    System.out.println("    -o <string>   outfile basename (mandatory)");
	    System.out.println("    -z            gzipped output file(s) (default: not set)");
	    System.out.println("    -c            z-score cutoff (defaut: 1.64)");
	    System.out.println("    -n            replacing read prefix/suffix to crop with Ns (default: not set)");
	    System.out.println("    -l <integer>  minimum length of output reads (default: 50)");
	    System.out.println("    -v            verbose mode (default: not set)");
	    System.out.println("    -h            prints this help and exit");
	    System.out.println("");
	    if ( args.length == 0 ) System.exit(1);
	    System.exit(0);
	}


	//#############################################################################################################
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	infq    = new File(NOTHING);  // -i
	infq1   = new File(NOTHING);  // -1
	infq2   = new File(NOTHING);  // -2
	paired  = false;
	outname = NOTHING;            // -o
	outgz   = false;              // -z
	cutoff  = 1.64;               // -c
	minlgt  = (short) 50;         // -l
	nocrop  = false;              // -n
	verbose = false;              // -v
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     {     infq = new File(args[++o]);                continue; }
	    if ( args[o].equals("-1") )     {    infq1 = new File(args[++o]); paired = true; continue; }
	    if ( args[o].equals("-2") )     {    infq2 = new File(args[++o]); paired = true; continue; }
	    if ( args[o].equals("-o") )     {  outname = args[++o];                          continue; }
	    if ( args[o].equals("-z") )     {    outgz = true;                               continue; }
	    if ( args[o].equals("-c") ) try {   cutoff = Double.parseDouble(args[++o]);      continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -c)"); System.exit(1); }
	    if ( args[o].equals("-l") ) try {   minlgt = Short.parseShort(args[++o]);        continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -l)"); System.exit(1); }
	    if ( args[o].equals("-n") )     {   nocrop = true;                               continue; }
	    if ( args[o].equals("-v") )     {  verbose = true;                               continue; }
	}
	if ( paired ) {
	    if ( infq1.toString().equals(NOTHING) ) { System.err.println("no input file (option -1)");                                 System.exit(1); }
	    if ( ! infq1.exists() )                 { System.err.println("file " + infq1.toString() + " does not exist (option -1)");  System.exit(1); }
	    if ( infq2.toString().equals(NOTHING) ) { System.err.println("no input file (option -2)");                                 System.exit(1); }
	    if ( ! infq2.exists() )                 { System.err.println("file " + infq2.toString() + " does not exist (option -2)");  System.exit(1); }
	}
	else {
	    if ( infq.toString().equals(NOTHING) )  { System.err.println("no input file (option -1)");                                 System.exit(1); }
	    if ( ! infq.exists() )                  { System.err.println("file " + infq .toString() + " does not exist (option -1)");  System.exit(1); }
	}	    
	if ( outname.equals(NOTHING) )              { System.err.println("no output file basename (option -o)");                       System.exit(1); }
	if ( minlgt < 1 )                           { System.err.println("minimum length should be positive (option -l)");             System.exit(1); }
	if ( cutoff < 0 )                           { System.err.println("z-score cutoff should be positive (option -c)");             System.exit(1); }
	if ( cutoff > 10 )                          { System.err.println("z-score cutoff should be lower than 10 (option -c)");        System.exit(1); }


	//#############################################################################################################
	//#############################################################################################################
	//### init variables                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	--minlgt;

	//#############################################################################################################
	//#############################################################################################################
	//### go!                                                                                                   ###
	//#############################################################################################################
	//#############################################################################################################
	if ( paired ) pe();
	else          se();
    }


    
    //#############################################################################################################
    //#############################################################################################################
    //### single-end                                                                                            ###
    //#############################################################################################################
    //#############################################################################################################
    final static void se() throws IOException {
	//#########################################################################################################
	//### first estimate of base proportion per position                                                    ###
	//#########################################################################################################
	filename = infq.toString();
	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	mlgt = 0; fA = new long[0]; fC = new long[0]; fG = new long[0]; fT = new long[0]; np = new long[0];
	cpt = 0;
	while ( ++cpt <= MODMASK && (l_1 = in.readLine()) != null ) {
	    l_2 = in.readLine();
	    l_3 = in.readLine(); 
	    l_4 = in.readLine();
	    //### read length ##########
	    lgt = (short) l_2.length();
	    if ( lgt <= minlgt ) { --cpt; continue; }
	    if ( lgt > mlgt ) {
		mlgt = lgt;
		fA = Arrays.copyOf(fA, mlgt); fC = Arrays.copyOf(fC, mlgt); fG = Arrays.copyOf(fG, mlgt); fT = Arrays.copyOf(fT, mlgt); np = Arrays.copyOf(np, mlgt);
	    }
	    //### counting bases ##########
	    p = S_1;
	    for (byte b: l_2.getBytes()) {
		++p;
		++np[p];
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA[p]; continue;
		case UTF8_C: case UTF8_c: ++fC[p]; continue;
		case UTF8_G: case UTF8_g: ++fG[p]; continue;
		case UTF8_T: case UTF8_t: ++fT[p]; continue;
		default:                  --np[p]; continue;
		}
	    }
	}
	in.close();
	//#########################################################################################################
	//### estimating initial statistics                                                                     ###
	//#########################################################################################################
	//## NOTE: here, mlgt = the (maximum) read length
	mA = med(fA, np); mC = med(fC, np); mG = med(fG, np); mT = med(fT, np);
	sum = mA + mC + mG + mT; mA /= sum; mC /= sum; mG /= sum; mT /= sum;
	System.out.println("");
	System.out.println("=== initial estimates ========================================================");
	dispStat(fA, fC, fG, fT, np, mlgt, mA, mC, mG, mT, false);
	//#########################################################################################################
	//### initial cropping                                                                                  ###
	//#########################################################################################################
	filename = infq.toString();
	in = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	filename = outname + ".fastq";
	out = ( outgz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER)), BUFFER) : Files.newBufferedWriter(Path.of(filename));
	thr = new double[mlgt];
	zS = new double[mlgt];
	hlgt = (short) (mlgt / 2);
	cpt = 0;
	while ( ++cpt <= MODMASK && (l_1 = in.readLine()) != null ) {
	    l_2 = in.readLine(); l_3 = in.readLine(); l_4 = in.readLine();
	    //### read length ##########
	    lgt = (short) l_2.length();
	    if ( lgt <= minlgt ) { --cpt; continue; }
	    //### estimating z-scores ##########
	    ba = l_2.getBytes();
	    p = S_1;
	    for (byte b: ba) {
		++p;
		n = np[p];
		thr[p] = ( fA[p] == 0 || fC[p] == 0 || fG[p] == 0 || fT[p] == 0 ) ? 0 : f(cpt, cutoff); 
		switch ( b ) {
		case UTF8_A: case UTF8_a: zS[p] = zscore(fA[p]/n, mA, n); continue;
		case UTF8_C: case UTF8_c: zS[p] = zscore(fC[p]/n, mC, n); continue;
		case UTF8_G: case UTF8_g: zS[p] = zscore(fG[p]/n, mG, n); continue;
		case UTF8_T: case UTF8_t: zS[p] = zscore(fT[p]/n, mT, n); continue;
		default:                  zS[p] = cutoff;                 continue;
		}
	    }
	    crop = discard = false;
	    //### inferring prefix to remove ##########
	    pl = crop5(zS, thr, lgt, hlgt);
	    crop = ( pl >= 0 );
	    //## NOTE: here, the prefix l_2[0,pl] should be removed
	    p = S_1;
	    while ( ++p <= pl ) {
		--np[p]; 
		switch ( ba[p] ) {
		case UTF8_A: case UTF8_a: --fA[p]; continue;
		case UTF8_C: case UTF8_c: --fC[p]; continue;
		case UTF8_G: case UTF8_g: --fG[p]; continue;
		case UTF8_T: case UTF8_t: --fT[p]; continue;
		default:                  ++np[p]; continue;
		}
	    }
	    //### inferring suffix to remove ##########
	    pr = crop3(zS, thr, lgt, hlgt);
	    crop |= ( pr < lgt );
	    //## NOTE: here, the suffix l_2[pr,lgt-1] should be removed
	    p = lgt;
	    while ( --p >= pr ) {
		--np[p]; 
		switch ( ba[p] ) {
		case UTF8_A: case UTF8_a: --fA[p]; continue;
		case UTF8_C: case UTF8_c: --fC[p]; continue;
		case UTF8_G: case UTF8_g: --fG[p]; continue;
		case UTF8_T: case UTF8_t: --fT[p]; continue;
		default:                  ++np[p]; continue;
		}
	    }
	    //### discarding the whole read ##########
	    if ( crop && pr - pl < minlgt ) {
		p = pl;
		while ( ++p < pr ) {
		    --np[p]; 
		    switch ( ba[p] ) {
		    case UTF8_A: case UTF8_a: --fA[p]; continue;
		    case UTF8_C: case UTF8_c: --fC[p]; continue;
		    case UTF8_G: case UTF8_g: --fG[p]; continue;
		    case UTF8_T: case UTF8_t: --fT[p]; continue;
		    default:                  ++np[p]; continue;
		    }
		}
		discard = true;
	    }
	    //### outputting (cropped) read ##########
	    if ( ! discard ) {
		if ( ! crop ) {
		    out.write(l_1); out.newLine();
		    out.write(l_2); out.newLine();
		    out.write(l_3); out.newLine();
		    out.write(l_4); out.newLine();
		}
		else {
		    ++pl;
		    if ( ! nocrop ) {
			out.write(l_1);                   out.newLine();
			out.write(l_2.substring(pl, pr)); out.newLine();
			out.write(l_3);                   out.newLine();
			out.write(l_4.substring(pl, pr)); out.newLine();
		    }
		    else {
			out.write(l_1);                                                                                           out.newLine();
			out.write(N1000.substring(0,pl)); out.write(l_2.substring(pl, pr)); out.write(N1000.substring(0,lgt-pr)); out.newLine();
			out.write(l_3);                                                                                           out.newLine();
			out.write(H1000.substring(0,pl)); out.write(l_4.substring(pl, pr)); out.write(H1000.substring(0,lgt-pr)); out.newLine();
		    }
		    --pl;
		}
	    }
	    if ( verbose && crop ) {
		System.out.print(l_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l_2);
		//++pl; System.out.println(N1000.substring(0,pl) + l_2.substring(pl, pr) + N1000.substring(0,lgt-pr)); --pl;
		p = S_1; while ( ++p < lgt ) System.out.print( ((zS[p] > thr[p]) ? "+" : (zS[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl ) System.out.print(">");
		--p;     while ( ++p <  pr ) System.out.print(" ");
		--p;     while ( ++p < lgt ) System.out.print("<");
		System.out.println("");
		System.out.println("");
	    }
	}
	//#########################################################################################################
	//### general cropping                                                                                  ###
	//#########################################################################################################
	--cpt;
	while ( (l_1 = in.readLine()) != null ) {
	    l_2 = in.readLine();
	    l_3 = in.readLine(); 
	    l_4 = in.readLine();
	    //### read length ##########
	    lgt = (short) l_2.length();
	    if ( lgt <= minlgt ) continue;
	    //### updating statistics ##########
	    if ( ((++cpt) & MODMASK) == 0 ) {
		mA = med(fA, np); mC = med(fC, np); mG = med(fG, np); mT = med(fT, np);
		sum = mA + mC + mG + mT; mA /= sum; mC /= sum; mG /= sum; mT /= sum;
		if ( verbose ) dispStat(fA, fC, fG, fT, np, mlgt, mA, mC, mG, mT, true);
	    }
	    //### counting bases and estimating z-scores ##########
	    ba = l_2.getBytes();
	    maybe = false;
	    p = S_1;
	    for (byte b: ba) {
		++p;
		n=(++np[p]);
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA[p]; zS[p] = zscore(fA[p]/n, mA, n); break;
		case UTF8_C: case UTF8_c: ++fC[p]; zS[p] = zscore(fC[p]/n, mC, n); break;
		case UTF8_G: case UTF8_g: ++fG[p]; zS[p] = zscore(fG[p]/n, mG, n); break;
		case UTF8_T: case UTF8_t: ++fT[p]; zS[p] = zscore(fT[p]/n, mT, n); break;
		default:                  --np[p]; zS[p] = cutoff;                 break;
		}
		thr[p] = ( fA[p] == 0 || fC[p] == 0 || fG[p] == 0 || fT[p] == 0 ) ? 0 : cutoff;
		maybe |= ( thr[p] == 0 || zS[p] > thr[p] );
	    }
	    crop = discard = false;
	    if ( maybe ) {
		//### inferring prefix to remove ##########
		pl = crop5(zS, thr, lgt, hlgt);
		crop = ( pl >= 0 );
		//## NOTE: here, the prefix l_2[0,pl] should be removed
		p = S_1;
		while ( ++p <= pl ) {
		    --np[p]; 
		    switch ( ba[p] ) {
		    case UTF8_A: case UTF8_a: --fA[p]; continue;
		    case UTF8_C: case UTF8_c: --fC[p]; continue;
		    case UTF8_G: case UTF8_g: --fG[p]; continue;
		    case UTF8_T: case UTF8_t: --fT[p]; continue;
		    default:                  ++np[p]; continue;
		    }
		}
		//### inferring suffix to remove ##########
		pr = crop3(zS, thr, lgt, hlgt);
		crop |= ( pr < lgt );
		//## NOTE: here, the suffix l_2[pr,lgt-1] should be removed
		p = lgt;
		while ( --p >= pr ) {
		    --np[p]; 
		    switch ( ba[p] ) {
		    case UTF8_A: case UTF8_a: --fA[p]; continue;
		    case UTF8_C: case UTF8_c: --fC[p]; continue;
		    case UTF8_G: case UTF8_g: --fG[p]; continue;
		    case UTF8_T: case UTF8_t: --fT[p]; continue;
		    default:                  ++np[p]; continue;
		    }
		}
	    }
	    //### discarding the whole read ##########
	    if ( crop && pr - pl < minlgt ) {
		p = pl;
		while ( ++p < pr ) {
		    --np[p]; 
		    switch ( ba[p] ) {
		    case UTF8_A: case UTF8_a: --fA[p]; continue;
		    case UTF8_C: case UTF8_c: --fC[p]; continue;
		    case UTF8_G: case UTF8_g: --fG[p]; continue;
		    case UTF8_T: case UTF8_t: --fT[p]; continue;
		    default:                  ++np[p]; continue;
		    }
		}
		discard = true;
	    }
	    //### outputting (cropped) read ##########
	    if ( ! discard ) {
		if ( ! crop ) {
		    out.write(l_1); out.newLine();
		    out.write(l_2); out.newLine();
		    out.write(l_3); out.newLine();
		    out.write(l_4); out.newLine();
		}
		else {
		    ++pl;
		    if ( ! nocrop ) {
			out.write(l_1);                   out.newLine();
			out.write(l_2.substring(pl, pr)); out.newLine();
			out.write(l_3);                   out.newLine();
			out.write(l_4.substring(pl, pr)); out.newLine();
		    }
		    else {
			out.write(l_1);                                                                                           out.newLine();
			out.write(N1000.substring(0,pl)); out.write(l_2.substring(pl, pr)); out.write(N1000.substring(0,lgt-pr)); out.newLine();
			out.write(l_3);                                                                                           out.newLine();
			out.write(H1000.substring(0,pl)); out.write(l_4.substring(pl, pr)); out.write(H1000.substring(0,lgt-pr)); out.newLine();
		    }
		    --pl;
		}
	    }
	    if ( verbose && crop ) {
		System.out.print(l_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l_2);
		p = S_1; while ( ++p < lgt ) System.out.print( ((zS[p] > thr[p]) ? "+" : (zS[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl ) System.out.print(">");
		--p;     while ( ++p <  pr ) System.out.print(" ");
		--p;     while ( ++p < lgt ) System.out.print("<");
		System.out.println("");
		System.out.println("");
	    }
	}
	in.close();
	out.close();
	
	System.out.println("=== final results ============================================================");
	dispStat(fA, fC, fG, fT, np, mlgt, mA, mC, mG, mT, true);
    }

    


    //#############################################################################################################
    //#############################################################################################################
    //### paired-ends                                                                                           ###
    //#############################################################################################################
    //#############################################################################################################
    final static void pe() throws IOException {
	//#########################################################################################################
	//### first estimate of base proportion per position                                                    ###
	//#########################################################################################################
	filename = infq1.toString();
	in1 = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	filename = infq2.toString();
	in2 = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	mlgt = 0;
	fA1 = new long[0]; fC1 = new long[0]; fG1 = new long[0]; fT1 = new long[0]; np1 = new long[0];
	fA2 = new long[0]; fC2 = new long[0]; fG2 = new long[0]; fT2 = new long[0]; np2 = new long[0];
	cpt = 0;
	while ( ++cpt <= MODMASK && (l1_1 = in1.readLine()) != null && (l1_2 = in2.readLine()) != null ) {
	    l1_2 = in1.readLine(); l1_3 = in1.readLine(); l1_4 = in1.readLine();
	    l2_2 = in2.readLine(); l2_3 = in2.readLine(); l2_4 = in2.readLine();
	    //### read length ##########
	    lgt1 = (short) l1_2.length();
	    lgt2 = (short) l2_2.length();
	    if ( lgt1 <= minlgt || lgt2 <= minlgt ) { --cpt; continue; }
	    if ( lgt1 > mlgt || lgt2 > mlgt ) {
		mlgt = (lgt1 < lgt2) ? lgt2 : lgt1;
		fA1 = Arrays.copyOf(fA1, mlgt); fC1 = Arrays.copyOf(fC1, mlgt); fG1 = Arrays.copyOf(fG1, mlgt); fT1 = Arrays.copyOf(fT1, mlgt); np1 = Arrays.copyOf(np1, mlgt);
		fA2 = Arrays.copyOf(fA2, mlgt); fC2 = Arrays.copyOf(fC2, mlgt); fG2 = Arrays.copyOf(fG2, mlgt); fT2 = Arrays.copyOf(fT2, mlgt); np2 = Arrays.copyOf(np2, mlgt);
	    }
	    //### counting bases ##########
	    p = S_1;
	    for (byte b: l1_2.getBytes()) {
		++p;
		++np1[p];
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA1[p]; continue;
		case UTF8_C: case UTF8_c: ++fC1[p]; continue;
		case UTF8_G: case UTF8_g: ++fG1[p]; continue;
		case UTF8_T: case UTF8_t: ++fT1[p]; continue;
		default:                  --np1[p]; continue;
		}
	    }
	    p = S_1;
	    for (byte b: l2_2.getBytes()) {
		++p;
		++np2[p];
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA2[p]; continue;
		case UTF8_C: case UTF8_c: ++fC2[p]; continue;
		case UTF8_G: case UTF8_g: ++fG2[p]; continue;
		case UTF8_T: case UTF8_t: ++fT2[p]; continue;
		default:                  --np2[p]; continue;
		}
	    }
	}
	in1.close();
	in2.close();
	//#########################################################################################################
	//### estimating initial statistics                                                                     ###
	//#########################################################################################################
	//## NOTE: here, mlgt = the (maximum) read length
	mA1 = med(fA1, np1); mC1 = med(fC1, np1); mG1 = med(fG1, np1); mT1 = med(fT1, np1);
	sum = mA1 + mC1 + mG1 + mT1; mA1 /= sum; mC1 /= sum; mG1 /= sum; mT1 /= sum;
	mA2 = med(fA2, np2); mC2 = med(fC2, np2); mG2 = med(fG2, np2); mT2 = med(fT2, np2);
	sum = mA2 + mC2 + mG2 + mT2; mA2 /= sum; mC2 /= sum; mG2 /= sum; mT2 /= sum;
	System.out.println("");
	System.out.println("=== initial estimates =====================================================================================================================================");
	dispStat(fA1, fC1, fG1, fT1, np1, mA1, mC1, mG1, mT1, mlgt, fA2, fC2, fG2, fT2, np2, mA2, mC2, mG2, mT2, false);
	//#########################################################################################################
	//### initial cropping                                                                                  ###
	//#########################################################################################################
	filename = outname + ".1.fastq";
	out1 = ( outgz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER)), BUFFER) : Files.newBufferedWriter(Path.of(filename));
	filename = outname + ".2.fastq";
	out2 = ( outgz ) ? new BufferedWriter(new OutputStreamWriter(new GZIPOutputStream(Files.newOutputStream(Path.of(filename + ".gz")), BUFFER)), BUFFER) : Files.newBufferedWriter(Path.of(filename));
	filename = infq1.toString();
	in1 = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	filename = infq2.toString();
	in2 = ( filename.endsWith(".gz") ) ? new BufferedReader(new InputStreamReader(new GZIPInputStream(Files.newInputStream(Path.of(filename)), BUFFER))) : Files.newBufferedReader(Path.of(filename));
	thr = new double[mlgt];
	zS = new double[mlgt];
	hlgt = (short) (mlgt / 2);
	cpt = 0;
	while ( ++cpt <= MODMASK && (l1_1 = in1.readLine()) != null && (l2_1 = in2.readLine()) != null ) {
	    l1_2 = in1.readLine(); l1_3 = in1.readLine(); l1_4 = in1.readLine();
	    l2_2 = in2.readLine(); l2_3 = in2.readLine(); l2_4 = in2.readLine();
	    //### read length ##########
	    lgt1 = (short) l1_2.length();
	    lgt2 = (short) l2_2.length();
	    if ( lgt1 <= minlgt || lgt2 <= minlgt ) { --cpt; continue; }
	    crop = discard = false;
	    //####################################################################
	    //### R1                                                           ###
	    //####################################################################
	    ba1 = l1_2.getBytes();
	    //### estimating z-scores ##########
	    p = S_1;
	    for (byte b: ba1) {
		++p;
		n = np1[p];
		thr[p] = ( fA1[p] == 0 || fC1[p] == 0 || fG1[p] == 0 || fT1[p] == 0 ) ? 0 : f(cpt, cutoff); 
		switch ( b ) {
		case UTF8_A: case UTF8_a: zS[p] = zscore(fA1[p]/n, mA1, n); continue;
		case UTF8_C: case UTF8_c: zS[p] = zscore(fC1[p]/n, mC1, n); continue;
		case UTF8_G: case UTF8_g: zS[p] = zscore(fG1[p]/n, mG1, n); continue;
		case UTF8_T: case UTF8_t: zS[p] = zscore(fT1[p]/n, mT1, n); continue;
		default:                  zS[p] = cutoff;                   continue;
		}
	    }
	    //### inferring prefix to remove ##########
	    pl1 = crop5(zS, thr, lgt1, hlgt);
	    crop = ( pl1 >= 0 );
	    //## NOTE: here, the prefix l1_2[0,pl1] should be removed
	    p = S_1;
	    while ( ++p <= pl1 ) {
		--np1[p]; 
		switch ( ba1[p] ) {
		case UTF8_A: case UTF8_a: --fA1[p]; continue;
		case UTF8_C: case UTF8_c: --fC1[p]; continue;
		case UTF8_G: case UTF8_g: --fG1[p]; continue;
		case UTF8_T: case UTF8_t: --fT1[p]; continue;
		default:                  ++np1[p]; continue;
		}
	    }
	    //### inferring suffix to remove ##########
	    pr1 = crop3(zS, thr, lgt1, hlgt);
	    crop |= ( pr1 < lgt1 );
	    //## NOTE: here, the suffix l1_2[pr1,lgt1-1] should be removed
	    p = lgt1;
	    while ( --p >= pr1 ) {
		--np1[p]; 
		switch ( ba1[p] ) {
		case UTF8_A: case UTF8_a: --fA1[p]; continue;
		case UTF8_C: case UTF8_c: --fC1[p]; continue;
		case UTF8_G: case UTF8_g: --fG1[p]; continue;
		case UTF8_T: case UTF8_t: --fT1[p]; continue;
		default:                  ++np1[p]; continue;
		}
	    }
	    if ( verbose ) { zS1 = Arrays.copyOf(zS, mlgt); thr1 = Arrays.copyOf(thr, mlgt); }
	    //####################################################################
	    //### R2                                                           ###
	    //####################################################################
	    ba2 = l2_2.getBytes();
	    //### estimating z-scores ##########
	    p = S_1;
	    for (byte b: ba2) {
		++p;
		n = np2[p];
		thr[p] = ( fA2[p] == 0 || fC2[p] == 0 || fG2[p] == 0 || fT2[p] == 0 ) ? 0 : f(cpt, cutoff); 
		switch ( b ) {
		case UTF8_A: case UTF8_a: zS[p] = zscore(fA2[p]/n, mA2, n); continue;
		case UTF8_C: case UTF8_c: zS[p] = zscore(fC2[p]/n, mC2, n); continue;
		case UTF8_G: case UTF8_g: zS[p] = zscore(fG2[p]/n, mG2, n); continue;
		case UTF8_T: case UTF8_t: zS[p] = zscore(fT2[p]/n, mT2, n); continue;
		default:                  zS[p] = cutoff;                   continue;
		}
	    }
	    //### inferring prefix to remove ##########
	    pl2 = crop5(zS, thr, lgt2, hlgt);
	    crop |= ( pl2 >= 0 );
	    //## NOTE: here, the prefix l2_2[0,pl2] should be removed
	    p = S_1;
	    while ( ++p <= pl2 ) {
		--np2[p]; 
		switch ( ba2[p] ) {
		case UTF8_A: case UTF8_a: --fA2[p]; continue;
		case UTF8_C: case UTF8_c: --fC2[p]; continue;
		case UTF8_G: case UTF8_g: --fG2[p]; continue;
		case UTF8_T: case UTF8_t: --fT2[p]; continue;
		default:                  ++np2[p]; continue;
		}
	    }
	    //### inferring suffix to remove ##########
	    pr2 = crop3(zS, thr, lgt2, hlgt);
	    crop |= ( pr2 < lgt2 );
	    //## NOTE: here, the suffix l2_2[pr2,lgt2-1] should be removed
	    p = lgt2;
	    while ( --p >= pr2 ) {
		--np2[p]; 
		switch ( ba2[p] ) {
		case UTF8_A: case UTF8_a: --fA2[p]; continue;
		case UTF8_C: case UTF8_c: --fC2[p]; continue;
		case UTF8_G: case UTF8_g: --fG2[p]; continue;
		case UTF8_T: case UTF8_t: --fT2[p]; continue;
		default:                  ++np2[p]; continue;
		}
	    }
	    if ( verbose ) { zS2 = Arrays.copyOf(zS, mlgt); thr2 = Arrays.copyOf(thr, mlgt); }
	    //####################################################################
	    //### R1 + R2                                                      ###
	    //####################################################################
	    //### discarding the whole reads ##########
	    if ( crop && (pr1 - pl1 < minlgt || pr2 - pl2 < minlgt) ) {
		p = pl1;
		while ( ++p < pr1 ) {
		    --np1[p]; 
		    switch ( ba1[p] ) {
		    case UTF8_A: case UTF8_a: --fA1[p]; continue;
		    case UTF8_C: case UTF8_c: --fC1[p]; continue;
		    case UTF8_G: case UTF8_g: --fG1[p]; continue;
		    case UTF8_T: case UTF8_t: --fT1[p]; continue;
		    default:                  ++np1[p]; continue;
		    }
		}
		p = pl2;
		while ( ++p < pr2 ) {
		    --np2[p]; 
		    switch ( ba2[p] ) {
		    case UTF8_A: case UTF8_a: --fA2[p]; continue;
		    case UTF8_C: case UTF8_c: --fC2[p]; continue;
		    case UTF8_G: case UTF8_g: --fG2[p]; continue;
		    case UTF8_T: case UTF8_t: --fT2[p]; continue;
		    default:                  ++np2[p]; continue;
		    }
		}
		discard = true;
	    }
	    //### outputting (cropped) reads ##########
	    if ( ! discard ) {
		if ( ! crop ) {
		    out1.write(l1_1); out1.newLine();
		    out1.write(l1_2); out1.newLine();
		    out1.write(l1_3); out1.newLine();
		    out1.write(l1_4); out1.newLine();
		    out2.write(l2_1); out2.newLine();
		    out2.write(l2_2); out2.newLine();
		    out2.write(l2_3); out2.newLine();
		    out2.write(l2_4); out2.newLine();
		}
		else {
		    ++pl1; ++pl2;
		    if ( ! nocrop ) {
			out1.write(l1_1);                     out1.newLine();
			out1.write(l1_2.substring(pl1, pr1)); out1.newLine();
			out1.write(l1_3);                     out1.newLine();
			out1.write(l1_4.substring(pl1, pr1)); out1.newLine();
			out2.write(l2_1);                     out2.newLine();
			out2.write(l2_2.substring(pl2, pr2)); out2.newLine();
			out2.write(l2_3);                     out2.newLine();
			out2.write(l2_4.substring(pl2, pr2)); out2.newLine();
		    }
		    else {
			out1.write(l1_1);                                                                                                  out1.newLine();
			out1.write(N1000.substring(0,pl1)); out1.write(l1_2.substring(pl1, pr1)); out1.write(N1000.substring(0,lgt1-pr1)); out1.newLine();
			out1.write(l1_3);                                                                                                  out1.newLine();
			out1.write(H1000.substring(0,pl1)); out1.write(l1_4.substring(pl1, pr1)); out1.write(H1000.substring(0,lgt1-pr1)); out1.newLine();
			out2.write(l2_1);                                                                                                  out2.newLine();
			out2.write(N1000.substring(0,pl2)); out2.write(l2_2.substring(pl2, pr2)); out2.write(N1000.substring(0,lgt2-pr2)); out2.newLine();
			out2.write(l2_3);                                                                                                  out2.newLine();
			out2.write(H1000.substring(0,pl2)); out2.write(l2_4.substring(pl2, pr2)); out2.write(H1000.substring(0,lgt2-pr2)); out2.newLine();
		    }
		    --pl1; --pl2;
		}
	    }
	    if ( verbose && crop ) {
		System.out.print(l1_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l1_2);
		p = S_1; while ( ++p < lgt1 ) System.out.print( ((zS1[p] > thr1[p]) ? "+" : (zS1[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl1 ) System.out.print(">");
		--p;     while ( ++p <  pr1 ) System.out.print(" ");
		--p;     while ( ++p < lgt1 ) System.out.print("<");
		System.out.println("");
		System.out.print(l2_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l2_2);
		p = S_1; while ( ++p < lgt2 ) System.out.print( ((zS2[p] > thr2[p]) ? "+" : (zS2[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl2 ) System.out.print(">");
		--p;     while ( ++p <  pr2 ) System.out.print(" ");
		--p;     while ( ++p < lgt2 ) System.out.print("<");
		System.out.println("");
		System.out.println("");
	    }
	}
	//#########################################################################################################
	//### general cropping                                                                                  ###
	//#########################################################################################################
	--cpt;
	while ( (l1_1 = in1.readLine()) != null && (l2_1 = in2.readLine()) != null ) {
	    l1_2 = in1.readLine(); l1_3 = in1.readLine(); l1_4 = in1.readLine();
	    l2_2 = in2.readLine(); l2_3 = in2.readLine(); l2_4 = in2.readLine();
	    //### read length ##########
	    lgt1 = (short) l1_2.length();
	    lgt2 = (short) l2_2.length();
	    if ( lgt1 <= minlgt || lgt2 <= minlgt ) continue;
	    //### updating statistics ##########
	    if ( ((++cpt) & MODMASK) == 0 ) {
		mA1 = med(fA1, np1); mC1 = med(fC1, np1); mG1 = med(fG1, np1); mT1 = med(fT1, np1);
		sum = mA1 + mC1 + mG1 + mT1; mA1 /= sum; mC1 /= sum; mG1 /= sum; mT1 /= sum;
		mA2 = med(fA2, np2); mC2 = med(fC2, np2); mG2 = med(fG2, np2); mT2 = med(fT2, np2);
		sum = mA2 + mC2 + mG2 + mT2; mA2 /= sum; mC2 /= sum; mG2 /= sum; mT2 /= sum;
		if ( verbose ) dispStat(fA1, fC1, fG1, fT1, np1, mA1, mC1, mG1, mT1, mlgt, fA2, fC2, fG2, fT2, np2, mA2, mC2, mG2, mT2, true);
	    }
	    crop = discard = false;
	    //####################################################################
	    //### R1                                                           ###
	    //####################################################################
	    ba1 = l1_2.getBytes();
	    //### counting bases and estimating z-scores ##########
	    maybe = false;
	    p = S_1;
	    for (byte b: ba1) {
		++p;
		n=(++np1[p]);
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA1[p]; zS[p] = zscore(fA1[p]/n, mA1, n); break;
		case UTF8_C: case UTF8_c: ++fC1[p]; zS[p] = zscore(fC1[p]/n, mC1, n); break;
		case UTF8_G: case UTF8_g: ++fG1[p]; zS[p] = zscore(fG1[p]/n, mG1, n); break;
		case UTF8_T: case UTF8_t: ++fT1[p]; zS[p] = zscore(fT1[p]/n, mT1, n); break;
		default:                  --np1[p]; zS[p] = cutoff;                   break;
		}
		thr[p] = ( fA1[p] == 0 || fC1[p] == 0 || fG1[p] == 0 || fT1[p] == 0 ) ? 0 : cutoff;
		maybe |= ( thr[p] == 0 || zS[p] > thr[p] );
	    }
	    pl1 = S_1; pr1 = lgt1;
	    if ( maybe ) {
		//### inferring prefix to remove ##########
		pl1 = crop5(zS, thr, lgt1, hlgt);
		crop = ( pl1 >= 0 );
		//## NOTE: here, the prefix l1_2[0,pl1] should be removed
		p = S_1;
		while ( ++p <= pl1 ) {
		    --np1[p]; 
		    switch ( ba1[p] ) {
		    case UTF8_A: case UTF8_a: --fA1[p]; continue;
		    case UTF8_C: case UTF8_c: --fC1[p]; continue;
		    case UTF8_G: case UTF8_g: --fG1[p]; continue;
		    case UTF8_T: case UTF8_t: --fT1[p]; continue;
		    default:                  ++np1[p]; continue;
		    }
		}
		//### inferring suffix(es) to remove ##########
		pr1 = crop3(zS, thr, lgt1, hlgt);
		crop |= ( pr1 < lgt1 );
		//## NOTE: here, the suffix l1_2[pr1,lgt1-1] should be removed
		p = lgt1;
		while ( --p >= pr1 ) {
		    --np1[p]; 
		    switch ( ba1[p] ) {
		    case UTF8_A: case UTF8_a: --fA1[p]; continue;
		    case UTF8_C: case UTF8_c: --fC1[p]; continue;
		    case UTF8_G: case UTF8_g: --fG1[p]; continue;
		    case UTF8_T: case UTF8_t: --fT1[p]; continue;
		    default:                  ++np1[p]; continue;
		    }
		}
	    }
	    if ( verbose ) { zS1 = Arrays.copyOf(zS, mlgt); thr1 = Arrays.copyOf(thr, mlgt); }
	    //####################################################################
	    //### R2                                                           ###
	    //####################################################################
	    ba2 = l2_2.getBytes();
	    //### counting bases and estimating z-scores ##########
	    maybe = false;
	    p = S_1;
	    for (byte b: ba2) {
		++p;
		n=(++np2[p]);
		switch ( b ) {
		case UTF8_A: case UTF8_a: ++fA2[p]; zS[p] = zscore(fA2[p]/n, mA2, n); break;
		case UTF8_C: case UTF8_c: ++fC2[p]; zS[p] = zscore(fC2[p]/n, mC2, n); break;
		case UTF8_G: case UTF8_g: ++fG2[p]; zS[p] = zscore(fG2[p]/n, mG2, n); break;
		case UTF8_T: case UTF8_t: ++fT2[p]; zS[p] = zscore(fT2[p]/n, mT2, n); break;
		default:                  --np2[p]; zS[p] = cutoff;                   break;
		}
		thr[p] = ( fA2[p] == 0 || fC2[p] == 0 || fG2[p] == 0 || fT2[p] == 0 ) ? 0 : cutoff;
		maybe |= ( thr[p] == 0 || zS[p] > thr[p] );
	    }
	    pl2 = S_1; pr2 = lgt2;
	    if ( maybe ) {
		//### inferring prefix(es) to remove ##########
		pl2 = crop5(zS, thr, lgt2, hlgt);
		crop |= ( pl2 >= 0 );
		//## NOTE: here, the prefix l2_2[0,pl2] should be removed
		p = S_1;
		while ( ++p <= pl2 ) {
		    --np2[p]; 
		    switch ( ba2[p] ) {
		    case UTF8_A: case UTF8_a: --fA2[p]; continue;
		    case UTF8_C: case UTF8_c: --fC2[p]; continue;
		    case UTF8_G: case UTF8_g: --fG2[p]; continue;
		    case UTF8_T: case UTF8_t: --fT2[p]; continue;
		    default:                  ++np2[p]; continue;
		    }
		}
		//### inferring suffix(es) to remove ##########
		pr2 = crop3(zS, thr, lgt2, hlgt);
		crop |= ( pr2 < lgt2 );
		//## NOTE: here, the suffix l2_2[pr2,lgt2-1] should be removed
		p = lgt2;
		while ( --p >= pr2 ) {
		    --np2[p]; 
		    switch ( ba2[p] ) {
		    case UTF8_A: case UTF8_a: --fA2[p]; continue;
		    case UTF8_C: case UTF8_c: --fC2[p]; continue;
		    case UTF8_G: case UTF8_g: --fG2[p]; continue;
		    case UTF8_T: case UTF8_t: --fT2[p]; continue;
		    default:                  ++np2[p]; continue;
		    }
		}
	    }
	    if ( verbose ) { zS2 = Arrays.copyOf(zS, mlgt); thr2 = Arrays.copyOf(thr, mlgt); }
	    //####################################################################
	    //### R1 + R2                                                       ###
	    //####################################################################
	    //### discarding the whole reads ##########
	    if ( crop && (pr1 - pl1 < minlgt || pr2 - pl2 < minlgt) ) {
		p = pl1;
		while ( ++p < pr1 ) {
		    --np1[p]; 
		    switch ( ba1[p] ) {
		    case UTF8_A: case UTF8_a: --fA1[p]; continue;
		    case UTF8_C: case UTF8_c: --fC1[p]; continue;
		    case UTF8_G: case UTF8_g: --fG1[p]; continue;
		    case UTF8_T: case UTF8_t: --fT1[p]; continue;
		    default:                  ++np1[p]; continue;
		    }
		}
		p = pl2;
		while ( ++p < pr2 ) {
		    --np2[p]; 
		    switch ( ba2[p] ) {
		    case UTF8_A: case UTF8_a: --fA2[p]; continue;
		    case UTF8_C: case UTF8_c: --fC2[p]; continue;
		    case UTF8_G: case UTF8_g: --fG2[p]; continue;
		    case UTF8_T: case UTF8_t: --fT2[p]; continue;
		    default:                  ++np2[p]; continue;
		    }
		}
		discard = true;
	    }
	    //### outputting (cropped) reads ##########
	    if ( ! discard ) {
		if ( ! crop ) {
		    out1.write(l1_1); out1.newLine();
		    out1.write(l1_2); out1.newLine();
		    out1.write(l1_3); out1.newLine();
		    out1.write(l1_4); out1.newLine();
		    out2.write(l2_1); out2.newLine();
		    out2.write(l2_2); out2.newLine();
		    out2.write(l2_3); out2.newLine();
		    out2.write(l2_4); out2.newLine();
		}
		else {
		    ++pl1; ++pl2;
		    if ( ! nocrop ) {
			out1.write(l1_1);                     out1.newLine();
			out1.write(l1_2.substring(pl1, pr1)); out1.newLine();
			out1.write(l1_3);                     out1.newLine();
			out1.write(l1_4.substring(pl1, pr1)); out1.newLine();
			out2.write(l2_1);                     out2.newLine();
			out2.write(l2_2.substring(pl2, pr2)); out2.newLine();
			out2.write(l2_3);                     out2.newLine();
			out2.write(l2_4.substring(pl2, pr2)); out2.newLine();
		    }
		    else {
			out1.write(l1_1);                                                                                                  out1.newLine();
			out1.write(N1000.substring(0,pl1)); out1.write(l1_2.substring(pl1, pr1)); out1.write(N1000.substring(0,lgt1-pr1)); out1.newLine();
			out1.write(l1_3);                                                                                                  out1.newLine();
			out1.write(H1000.substring(0,pl1)); out1.write(l1_4.substring(pl1, pr1)); out1.write(H1000.substring(0,lgt1-pr1)); out1.newLine();
			out2.write(l2_1);                                                                                                  out2.newLine();
			out2.write(N1000.substring(0,pl2)); out2.write(l2_2.substring(pl2, pr2)); out2.write(N1000.substring(0,lgt2-pr2)); out2.newLine();
			out2.write(l2_3);                                                                                                  out2.newLine();
			out2.write(H1000.substring(0,pl2)); out2.write(l2_4.substring(pl2, pr2)); out2.write(H1000.substring(0,lgt2-pr2)); out2.newLine();
		    }
		    --pl1; --pl2;
		}
	    }
	    if ( verbose && crop ) {
		System.out.print(l1_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l1_2);
		p = S_1; while ( ++p < lgt1 ) System.out.print( ((zS1[p] > thr1[p]) ? "+" : (zS1[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl1 ) System.out.print(">");
		--p;     while ( ++p <  pr1 ) System.out.print(" ");
		--p;     while ( ++p < lgt1 ) System.out.print("<");
		System.out.println("");
		System.out.print(l2_1); System.out.println( ((discard) ? "    [DISCARDED]" : "") );
		System.out.println(l2_2);
		p = S_1; while ( ++p < lgt2 ) System.out.print( ((zS2[p] > thr2[p]) ? "+" : (zS2[p] < -cutoff) ? "-" : " ") );
		System.out.println("");
		p = S_1; while ( ++p <= pl2 ) System.out.print(">");
		--p;     while ( ++p <  pr2 ) System.out.print(" ");
		--p;     while ( ++p < lgt2 ) System.out.print("<");
		System.out.println("");
		System.out.println("");
	    }
	}
	in1.close();
	in2.close();
	out1.close();
	out2.close();
	
	System.out.println("=== final results =========================================================================================================================================");
	dispStat(fA1, fC1, fG1, fT1, np1, mA1, mC1, mG1, mT1, mlgt, fA2, fC2, fG2, fT2, np2, mA2, mC2, mG2, mT2, true);
    }
	
	
    static final short crop5 (final double[] zScore, final double[] threshold, final short end, final short mid) {
	short p5 = S_1;
	final short up = ( end < mid ) ? end : mid;
	//## NOTE: mandatory 5' cropping when some base is missing, i.e. threshold == 0
	short p = S_1;
	while ( ++p < end && threshold[p] == 0 ) {}
	p5 = (--p);
	//## NOTE: z-score-based 5' cropping, i.e. p such that sum[1..p] z-score[p] is positive and maximized
	double max = 0;
	double sum = 0;
	p = S_1; while ( ++p < up ) if ( (sum += lz(zScore[p])) > max && zScore[p] > threshold[p] ) { max = sum; p5 = p; }		
	return p5;
    }

    static final short crop3 (final double[] zScore, final double[] threshold, final short end, final short mid) {
	short p3 = S_1;	
	//## NOTE: mandatory 3' cropping when some base is missing, i.e. threshold == 0
	short p = end;
	while ( --p >= 0 && threshold[p] == 0 ) {}
	p3 = (++p); 
	//## NOTE: z-score-based 3' cropping, i.e. p such that sum[1..p] z-score[p] is positive and maximized
	double max = 0;
	double sum = 0;
	p = end; while ( --p >= mid ) if ( (sum += lz(zScore[p])) > max && zScore[p] > threshold[p] ) { max = sum; p3 = p; }
	return p3;
    }

    // returns the median of a1/a2
    static final double med (final long[] a1, final long[] a2) {
	final int lg = a1.length;
	final double[] sda = new double[lg];
	int i = lg; while ( --i >= 0 ) sda[i] = a1[i] / (double) a2[i];
	Arrays.sort(sda);
	return ( lg%2 == 1 ) ? sda[lg/2] : (sda[lg/2]+sda[1+lg/2])/2;
    }

    // returns the one-proportion Z test statistics
    //  op = observed proportion
    //  ep = expected proprotion
    //  ss = sample size
    static final double zscore (final double op, final double ep, final double ss) {
	return (op - ep) / Math.sqrt(ep*(1-ep)/ss);
    }

    // returns 1.5x the specified value when negative
    static final double lz (final double x) {
	return ( x < 0 ) ? 1.5 * x : x;
    }

    // returns the value of f(x) = c + 10 * exp(-a * x + log(1 - c/10))
    //  f(x) is a decreasing function such that f(0) = 10 and f(x) ~ c when x becomes large
    //  the constant a = 0.001 allows observing a moderate convergence to f(x) = c
    static final double f (final double x, final double c) {
	return c + 10.0 * Math.exp(-0.001 * x + Math.log(1.0 - c/10.0));
    }


    static final void dispStat(final long[] fA, final long[] fC, final long[] fG, final long[] fT, final long[] np, final int mlgt, final double mA, final double mC, final double mG, final double mT, final boolean dispNbases) {
	System.out.println("");
	System.out.println("           fA     fC     fG     fT");
	System.out.print("median");
	System.out.print(String.format(Locale.US, "  %.3f", mA));
	System.out.print(String.format(Locale.US, "  %.3f", mC));
	System.out.print(String.format(Locale.US, "  %.3f", mG));
	System.out.print(String.format(Locale.US, "  %.3f", mT));
	System.out.println("");
	System.out.println("");
	System.out.println( ((dispNbases)
			     ? "pos        fA     fC     fG     fT       zA      zC      zG      zT     nbases"
			     : "pos        fA     fC     fG     fT       zA      zC      zG      zT") );
	int p = -1;
	while ( ++p < mlgt ) {
	    System.out.print(((p+1) + "       ").substring(0,6));
	    System.out.print( ((fA[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fA[p]/(double)np[p])) );
	    System.out.print( ((fC[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fC[p]/(double)np[p])) );
	    System.out.print( ((fG[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fG[p]/(double)np[p])) );
	    System.out.print( ((fT[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fT[p]/(double)np[p])) );
	    System.out.print(" ");
	    System.out.print(String.format(Locale.US, "%1$8s", (fA[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fA[p]/(double)np[p], mA, np[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fC[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fC[p]/(double)np[p], mC, np[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fG[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fG[p]/(double)np[p], mG, np[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fT[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fT[p]/(double)np[p], mT, np[p]))));
	    System.out.print( ((dispNbases) ? String.format(Locale.US, " %1$10s", np[p]) : "") );
	    System.out.println("");
	}
	System.out.println("");
    }

    static final void dispStat(final long[] fA1, final long[] fC1, final long[] fG1, final long[] fT1, final long[] np1, final double mA1, final double mC1, final double mG1, final double mT1, 
			       final int mlgt,
			       final long[] fA2, final long[] fC2, final long[] fG2, final long[] fT2, final long[] np2, final double mA2, final double mC2, final double mG2, final double mT2,
			       final boolean dispNbases) {
	System.out.println("");
	System.out.println("        [  fA     fC     fG     fT                                         R1]       [  fA     fC     fG     fT                                         R2]");
	System.out.print("median");
	System.out.print(String.format(Locale.US, "  %.3f", mA1));
	System.out.print(String.format(Locale.US, "  %.3f", mC1));
	System.out.print(String.format(Locale.US, "  %.3f", mG1));
	System.out.print(String.format(Locale.US, "  %.3f", mT1));
	System.out.print("                                                 ");
	System.out.print(String.format(Locale.US, "  %.3f", mA2));
	System.out.print(String.format(Locale.US, "  %.3f", mC2));
	System.out.print(String.format(Locale.US, "  %.3f", mG2));
	System.out.print(String.format(Locale.US, "  %.3f", mT2));
	System.out.println("");
	System.out.println("");
	System.out.println( ((dispNbases)
			     ? "pos        fA     fC     fG     fT       zA      zC      zG      zT     nbases          fA     fC     fG     fT       zA      zC      zG      zT     nbases"
			     : "pos        fA     fC     fG     fT       zA      zC      zG      zT                     fA     fC     fG     fT       zA      zC      zG      zT") );
	int p = -1;
	while ( ++p < mlgt ) {
	    System.out.print(((p+1) + "       ").substring(0,6));
	    System.out.print( ((fA1[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fA1[p]/(double)np1[p])) );
	    System.out.print( ((fC1[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fC1[p]/(double)np1[p])) );
	    System.out.print( ((fG1[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fG1[p]/(double)np1[p])) );
	    System.out.print( ((fT1[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fT1[p]/(double)np1[p])) );
	    System.out.print(" ");
	    System.out.print(String.format(Locale.US, "%1$8s", (fA1[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fA1[p]/(double)np1[p], mA1, np1[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fC1[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fC1[p]/(double)np1[p], mC1, np1[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fG1[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fG1[p]/(double)np1[p], mG1, np1[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fT1[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fT1[p]/(double)np1[p], mT1, np1[p]))));
	    System.out.print( ((dispNbases) ? String.format(Locale.US, " %1$10s", np1[p]) : "           ") );
    	    System.out.print("     ");
	    System.out.print( ((fA2[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fA2[p]/(double)np2[p])) );
	    System.out.print( ((fC2[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fC2[p]/(double)np2[p])) );
	    System.out.print( ((fG2[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fG2[p]/(double)np2[p])) );
	    System.out.print( ((fT2[p] == 0) ? "  0    " : String.format(Locale.US, "  %.3f", fT2[p]/(double)np2[p])) );
	    System.out.print(" ");
	    System.out.print(String.format(Locale.US, "%1$8s", (fA2[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fA2[p]/(double)np2[p], mA2, np2[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fC2[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fC2[p]/(double)np2[p], mC2, np2[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fG2[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fG2[p]/(double)np2[p], mG2, np2[p]))));
	    System.out.print(String.format(Locale.US, "%1$8s", (fT2[p] == 0) ? "." : String.format(Locale.US, "%.2f", zscore(fT2[p]/(double)np2[p], mT2, np2[p]))));
	    System.out.print( ((dispNbases) ? String.format(Locale.US, " %1$10s", np2[p]) : "") );
	    System.out.println("");
	}
	System.out.println("");
    }
}
